//! Use a hopsets labelling to provide a distance oracle

use std::collections::HashMap;

use petgraph::algo::dijkstra;

use crate::errors::ErrorKind;
use crate::graph::{to_graphmap, Distance, Graph, Node};

/// Use petgraph::dijkstra to provide a reference distance oracle in the base graph
pub fn dijkstra_oracle(graph: &Graph, from: Node, to: Node) -> Result<Distance, ErrorKind> {
    let petgraph_result: HashMap<Node, Distance> =
        dijkstra(&to_graphmap(graph), from, Some(to), |edge| *(edge.2) as u64);

    petgraph_result
        .get(&to)
        .ok_or(ErrorKind::NotConnected(from, to))
        .map(|f| *f)
}

/// Use the hopset decomposition into g1 and g2 to find the distance between u and v
///
/// Assumptions:
///  - if u == v, dist(u, v) = 0
///  - if (u, v) is in g1, then taking this edge is the shortest path from u to v
///  - there exists x and y such that (u, x) in g1, (x, y) in g2, (y, v) in g1
///  - the distance between u and v is the minimum of such paths
/// Warning: this does not make the assumption that (u, u) is in g2 for every node u
///
/// Note: this function does not fail, but might returns `Distance::MAX` if the assumptions are not
/// verified.
///
/// Example:
/// ```
/// # use rhopsets::graph::Graph;
/// # use rhopsets::oracle::hopsets_oracle;
///
/// let g1 = Graph::from_edges(vec![(0, 1, 1), (2, 3, 1)].into_iter());
/// let g2 = Graph::from_edges(vec![(1, 2, 3)].into_iter());
///
/// assert_eq!(hopsets_oracle(&g1, &g2, 0, 3), 5);
/// ```
///
pub fn hopsets_oracle(g1: &Graph, g2: &Graph, u: Node, v: Node) -> Distance {
    let mut mini = Distance::MAX;

    if u == v {
        return Distance::default();
    }

    if g1.contains_edge(u, v) {
        return g1.edge_weight(u, v).unwrap_or(Distance::MAX);
    }

    for (_, x, d_ux) in g1.edges(u) {
        for (_, y, d_vy) in g1.edges(v) {
            if x == y {
                let d_via_x = d_ux + d_vy;
                if mini > d_via_x {
                    mini = d_via_x;
                }
            } else if let Some(d_xy) = g2.edge_weight(x, y) {
                let d_via_xy = d_ux + d_xy + d_vy;
                if mini > d_via_xy {
                    mini = d_via_xy;
                }
            }
        }
    }

    return mini;
}
