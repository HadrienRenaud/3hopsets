//! A very basic draw module for the graph library. May be outdated.

use std::io::Error;
use std::io::Write;

use crate::graph::{Graph, Distance};

/// Draw a superposition of 3 graphs into a buffer with graphviz syntax.
pub fn draw_graph_hopsets<T>(
    buf: &mut T,
    graph: &Graph,
    g1: &Graph,
    g2: &Graph,
) -> Result<(), Error>
where
    T: Write,
{
    writeln!(buf, "graph {{")?;
    writeln!(buf, "\tlayout=dot")?;

    for u in graph.nodes() {
        writeln!(buf, "\t{} [ label = \"{}\" ];", u, u)?;
    }

    for (u, v, w) in graph.all_edges() {
        writeln!(
            buf,
            "\t{} -- {} [ label=\"{}\" color=grey len={} ];",
            u, v, w, w
        )?;
    }
    for (u, v, w) in g1.all_edges() {
        if !graph.contains_edge(u, v) {
            writeln!(buf, "\t{} -- {} [ label=\"{}\" color=blue ];", u, v, w)?;
        } else if graph.edge_weight(u, v).unwrap_or(Distance::MAX) != w {
            log::error!(
                "Warning: Incoherent weights for edge {} -> {} between g and g1",
                u,
                v
            );
        }
    }
    for (u, v, w) in g2.all_edges() {
        writeln!(buf, "\t{} -- {} [ label=\"{}\" color=red ];", u, v, w)?;
    }

    writeln!(buf, "}}")?;

    Ok(())
}
