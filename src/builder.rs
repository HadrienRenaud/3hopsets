//! An algorithm to build a 3 hopsets decomposition.
//!
//! The main entry point is `build_three_hopsets`, which compute a 3 hopsets decomposition of a
//! a graph.
//! On a large picture, the algorithm is the following:
//! ```ignore
//! fun build_three_hopsets(graph: &Graph, d_: f64, epsilon: f64) -> Result<(), ErrorKind> {
//!     let rhos = construct_rhos(graph.node_count());
//!     let mut r1 = Vec::new();
//!     let mut r2 = Vec::new();
//!
//!     for u in graph.nodes() {
//!         let (e1, e2) = build_from_node(u, graph, rhos, d_, epsilon);
//!         r1.extend(e1);
//!         r2.extend(e1);
//!     }
//!
//!     let g1 = Graph::from_edges(r1);
//!     let g2 = Graph::from_edges(r2);
//!
//!     return (g1, g2);
//! }
//! ```

use std::iter::FromIterator;

use log;
use rand;
use rand::Rng;

use crate::dijkstra::dijkstra;
use crate::errors::ErrorKind;
use crate::graph::{Distance, Edge, Graph, Node};
use crate::parser::export_hopsets_edges;
use rayon::prelude::*;

pub type Rho = f32;
pub type Rhos = Vec<f32>;
type Edges = Vec<Edge>;

const USE_REAL_D: bool = false;
const BUILD_GRAPHS: bool = cfg!(test);

const ZERO_DISTANCE: Distance = 0.0 as Distance;
const ZERO_RHO: Rho = 0.0 as Rho;
const ZERO_NODE: Node = 0 as Node;

/// Returns the node minimizing rho and its distance to u along the path (u, v) with distance constraints
///
/// Takes as parameters:
///  - a vec that maps each node to its distance to u
///  - a predecessor vec that maps each node to a predecessor in the path, as returned by `dijkstra`
///  - a vec that maps elements to their costs
///  - u the start of the path
///  - v the end of the path
///  - d_min the minimum distance for x
///  - d_max the maximum distance for x
///
/// If there is a node x such that d_min < d_ux < d_max, then the returned value d_min_rho
///     verifies the inequality too: d_min < d_min_rho < d_max, and that the node x returned
///     minimies rho along the sub-path.
///
/// This function just iterates over the nodes in the path, so is linear in the length of the path.
///
/// This function is tested in the submodule `tests_min_rho_along_path`.
fn min_rho_along_path(
    paths_lengths: &Vec<Distance>,
    predecessor: &Vec<Option<Node>>,
    rhos: &Rhos,
    u: Node,
    v: Node,
    d_min: Distance,
    d_max: Distance,
) -> (Node, Distance) {
    // We iterate over the elements of the path
    // We have to store multiples information along this iteration, so we declare a lot of mutable
    // variables

    // w is the node inspected at this iteration of the loop
    let mut w: Node = v;
    let mut rho_w: Rho = *rhos.get(w).unwrap_or(&ZERO_RHO);
    let mut d_uw: Distance = *paths_lengths.get(w).unwrap_or(&ZERO_DISTANCE);

    // Old w keeps track of the previous values in the loop
    // These values will be initialized at the first iteration of the loop
    let mut old_w: Node;
    let mut old_rho_w: Rho;
    let mut old_d_uw: Distance;

    // imin_rho keeps track of the minimum took by w on its iterations
    let mut imin_rho = v;
    let mut min_rho = Rho::MAX;
    let mut dmin_rho = d_uw;

    // has_entered_zone is a flag that is set to true if the algorithm finds an element in
    let mut has_entered_zone = false;

    if d_max >= d_uw {
        has_entered_zone = true;
        min_rho = rho_w;
        // dmin_rho = d_uw;
    }

    // Main loop
    while w != u {
        // Iterate on the path
        old_w = w;
        old_rho_w = rho_w;
        old_d_uw = d_uw;

        // as the path is connected, it does not raise any error
        w = predecessor.get(w).unwrap().unwrap_or(ZERO_NODE);
        rho_w = *rhos.get(w).unwrap_or(&ZERO_RHO);
        d_uw = *paths_lengths.get(w).unwrap_or(&ZERO_DISTANCE);

        // If d_uw < d_min, it means that we can stop, as the resulting nodes don't gives any
        // useful informations here.
        if d_uw < d_min {
            // A special case is when the loop has not entered the (d_min, d_max) zone.
            if has_entered_zone {
                return (imin_rho, dmin_rho);
            } else {
                if rho_w < old_rho_w {
                    return (w, d_uw);
                } else {
                    return (old_w, old_d_uw);
                }
            }
        }

        // If we are in the (d_min, d_max) zone, we can update our minimum
        if d_uw <= d_max {
            if rho_w < min_rho {
                min_rho = rho_w;
                imin_rho = w;
                dmin_rho = d_uw;
            }

            has_entered_zone = true;
        }
    }

    return (imin_rho, dmin_rho);
}

/// find_d compute d for a given d', d_uv and epsilon
///
/// Furthermore, it takes as parameters:
/// - un_epsilon = 1 + epsilon
/// - ln_un_epsilon = ln(1 + epsilon)
/// - ln_ln_1p_d_ = ln(1 + ln(d'))
fn find_d(d_uv: f64, d_: f64, un_epsilon: f64, ln_un_epsilon: f64, ln_ln_1p_d_: f64) -> f64 {
    if USE_REAL_D {
        return d_uv;
    } else {
        let i = ((d_uv.ln().ln_1p() - ln_ln_1p_d_) / ln_un_epsilon).floor();
        return d_.powf(un_epsilon.powf(i));
    }
}

/// Build hopsets starting at u in the given graph, following the rhos ordering
/// It returns the constructed edges in g1 and g2
///
/// Uses:
/// - Shortest paths are computed with dijkstra
/// - min_rho_along_path to select a hop point on a given shortest path
fn build_from_node(u: Node, graph: &Graph, rhos: &Rhos, d_: f64, epsilon: f64) -> (Edges, Edges) {
    log::debug!("Building paths from u={}", u);

    log::debug!("Computing shortests paths ...");
    let (lengths, preds): (Vec<Distance>, Vec<Option<Node>>) = dijkstra(graph, u);

    // Precompute things for d
    let un_epsilon: f64 = 1f64 + epsilon;
    let ln_un_epsilon: f64 = epsilon.ln_1p();
    let ln_ln_1p_d_: f64 = d_.ln().ln_1p();

    let mut r1 = Edges::new();
    let mut r2 = Edges::new();

    log::debug!("Processing edges ...");
    for v in 0..graph.node_count() {
        // If u == v, then the oracle know that dist(u, v) == 0
        // If (u, v) is in graph, then implicitly (u, v) in g1
        // This argument will apply every time that we will want to apply f1
        if u == v || graph.contains_edge(u, v) {
            continue;
        }

        // If u < v, we can ignore this because:
        //      - the graph is undirected
        //      - v -> u will be considered
        if u < v {
            continue;
        }

        let d_uv: Distance = *lengths.get(v).unwrap_or(&Distance::default());

        // Treat as classical
        if d_uv < (d_ as Distance) {
            let (x, d_ux) =
                min_rho_along_path(&lengths, &preds, rhos, u, v, d_uv / 3, d_uv * 2 / 3 + 1);

            log::debug!("For {} -> {} ({}), found x={} ({})", u, v, d_uv, x, d_ux);

            if !graph.contains_edge(u, x) {
                r1.push((u, x, d_ux));
            };
            if !graph.contains_edge(x, v) {
                r1.push((x, v, d_uv - d_ux));
            };
        }
        // Build 3 hopsets
        else {
            let d = find_d(d_uv as f64, d_, un_epsilon, ln_un_epsilon, ln_ln_1p_d_);

            let (x, d_ux) = min_rho_along_path(
                &lengths,
                &preds,
                rhos,
                u,
                v,
                (d * 0.25) as Distance,
                (d * 0.5) as Distance,
            );
            let (y, d_uy) = min_rho_along_path(
                &lengths,
                &preds,
                rhos,
                u,
                v,
                (d * 0.5) as Distance,
                (d * 0.75) as Distance,
            );

            log::debug!(
                "For {}->{} ({}), found x={}({}) and y={}({})",
                u,
                v,
                d_uv,
                x,
                d_ux,
                y,
                d_uy
            );

            if !graph.contains_edge(u, x) {
                r1.push((u, x, d_ux));
            };
            if y != x {
                r2.push((x, y, d_uy - d_ux));
            };
            if !graph.contains_edge(y, v) {
                r1.push((y, v, d_uv - d_uy));
            };
        }
    }

    return (r1, r2);
}

/// Construct a total ordering on nodes, by associating them to a random number
fn construct_rhos(n: Node) -> Rhos {
    log::info!("Construct rhos ...");
    let mut rhos = Rhos::with_capacity(n as usize);

    let mut rng = rand::thread_rng();
    for _ in 0..n {
        let rho: f32 = rng.gen();
        rhos.push(rho);
    }

    rhos
}

/// Return an edge so that it can be sorted
/// As graphs are undirected, an edge is always represented as a tuple (u, v) with u < v
#[inline]
fn edge_to_canonical_edge((u, v, w): Edge) -> Edge {
    if u < v {
        (u, v, w)
    } else {
        (v, u, w)
    }
}

/// This function test the equality between edges,
/// with the assumption that edges are sorted, ie that u1 < v1 and u2 < v2
/// This function forgets to test if weights are equals, if the graph is consistent
/// and our algorithms are corrects, this should be true
#[inline]
fn are_two_edges_equals((u1, v1, _): &mut Edge, (u2, v2, _): &mut Edge) -> bool {
    u1 == u2 && v1 == v2
}

/// Function that forgets an edge weight to return only its start and destination
#[inline]
fn edge_key((u, v, _): &Edge) -> (Node, Node) {
    (*u, *v)
}

/// Build a 3 hopset labeling for the given graph
///
/// The paths for which distance is bellow d_ will be just 2-hop labelled. It adds the implicits
/// edges for the 2 graphs (ie g1 includes graph, and g2 has a self loop for every node).
///
/// It uses rayon::ParallelIterator to take advantage of parallelism:
///  - for each node u, it uses `build_from_node`
///  - the ordering is precomputed, and not parallelized
///  - graph reconstruction from the edges given by `build_from_nodes` is done in parallel
///     for g1 and g2
pub fn build_three_hopsets(
    graph: &Graph,
    d_: f64,
    epsilon: f64,
) -> Result<(Graph, Graph), ErrorKind> {
    log::info!("Input graph : {}", graph);

    let n = graph.node_count();

    // Construct rhos
    let rhos = construct_rhos(graph.node_count());

    // Allocate vectors to store the results of the `build_from_node` operations for every node.
    // We add 1 to the capacity as we will need to add the implicit edges to both graphs
    let mut r1 = Vec::<Edges>::with_capacity(n + 1);
    let mut r2 = Vec::<Edges>::with_capacity(n + 1);

    log::info!("Parse paths from nodes (in parallel) ...");
    // Run everything with rayon to parallelize
    // Here we suppose that node identifiers are connex, ie that :
    //      graph.nodes == 0..graph.node_counts()
    // We can't use directly graph.nodes() because it does not implement IntoParallelIterator
    (0..n)
        // Convert into a parallel iterator
        .into_par_iter()
        // Handle panics at global level
        .panic_fuse()
        // Do the real work
        // We use here map_with, which should avoid copying graph and rhos every time
        .map_with((&graph, &rhos, d_, epsilon), |(g, r, d, e), u| {
            build_from_node(u, g, r, *d, *e)
        })
        // Convert to a vec
        .unzip_into_vecs(&mut r1, &mut r2);

    // Add implicit edges to every graph
    // Then build the graph from the edges
    // We do that in parallel for g1 and g2
    log::info!("Collecting resulting edges...");
    let (mut edges1, mut edges2): (Edges, Edges) = rayon::join(
        || {
            // g1 contains g implicitly
            r1.push(Edges::from_iter(graph.all_edges()));
            r1.into_iter()
                .flatten()
                // Transform to canonical edges. That will allow us to simply test edge equality after.
                .map(edge_to_canonical_edge)
                .collect::<Edges>()
        },
        || {
            // g2 contains (u,u) for every node implicitly
            r2.push(Edges::from_iter(
                graph.nodes().map(|u| (u, u, Distance::default())),
            ));
            r2.into_iter()
                .flatten()
                .map(edge_to_canonical_edge)
                .collect::<Edges>()
        },
    );

    log::info!("Removing duplicated edges ...");
    // Removing duplicated edges is done by first sorting the keys, then removing the duplicates
    // It is faster because we use par_sort which uses all the cores
    // As we have applied

    let m_1 = edges1.len();
    let m_2 = edges2.len();

    // We cannot parallelize here, as par_sort already uses all cores
    // We dont need to use sorted_edge_key, as the edges are already sorted in edges1 and edges2
    edges1.par_sort_unstable_by_key(edge_key);
    edges2.par_sort_unstable_by_key(edge_key);

    // Then we remove the duplicates in edges1 and edges2
    // Now that they are sorted, duplicates are consecutive so dedup is enough to remove duplicates
    // Edge direction does not matter here as edges are in canonical form (ie u < v)
    rayon::join(
        || edges1.dedup_by(are_two_edges_equals),
        || edges2.dedup_by(are_two_edges_equals),
    );

    log::info!(
        "Passed from {} to {} edges by dedubing in g1",
        m_1,
        edges1.len()
    );
    log::info!(
        "Passed from {} to {} edges by dedubing in g2",
        m_2,
        edges2.len()
    );

    if BUILD_GRAPHS {
        Ok(rayon::join(
            || Graph::from_edges_with_capacity(n, edges1.len(), edges1.into_iter()),
            || Graph::from_edges_with_capacity(n, edges2.len(), edges2.into_iter()),
        ))
    } else {
        export_hopsets_edges(edges1.into_iter(), edges2.into_iter())?;
        return Ok((Graph::new(), Graph::new()));
    }
}

#[cfg(test)]
mod tests_min_rho_along_path {
    use super::*;
    use rand::random;
    use std::iter::FromIterator;

    #[test]
    fn test_with_basic_path() {
        let path_lengths: Vec<Distance> = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        let predecessor: Vec<Option<Node>> =
            Vec::from_iter((0..10).map(|i| if i == 0 { None } else { Some(i - 1) }));
        let rhos: Rhos = (0..(10 as usize)).map(|i| i as Rho).collect();

        let result = min_rho_along_path(&path_lengths, &predecessor, &rhos, 0, 9, 3, 7);
        assert_eq!(result, (3, 3));
    }

    #[test]
    fn test_with_reversed_path() {
        let path_lengths: Vec<Distance> = vec![9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
        let predecessor: Vec<Option<Node>> =
            Vec::from_iter((0..10).map(|i| if i == 9 { None } else { Some(i + 1) }));
        let rhos: Rhos = (0..10).map(|i| i as Rho).collect();

        let result = min_rho_along_path(&path_lengths, &predecessor, &rhos, 9, 0, 3, 7);
        assert_eq!(result, (2, 7));
    }

    #[test]
    fn test_with_random_path() {
        let path_lengths: Vec<Distance> = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        let predecessor: Vec<Option<Node>> =
            Vec::from_iter((0..10).map(|i| if i == 0 { None } else { Some(i - 1) }));

        for _ in 0..10 {
            let rhos: Rhos = (0..10).map(|_| random()).collect();
            let (i, d) = min_rho_along_path(&path_lengths, &predecessor, &rhos, 0, 9, 3, 7);
            let expected_r = (3..8)
                .map(|i| rhos[i])
                .min_by(|a, b| a.partial_cmp(b).expect("Only have valid values here."))
                .expect("There exists a minimum in this array");
            let r = rhos[i];
            assert_eq!(r, expected_r, "Wrong minimum for rhos={:#?}", rhos);
            assert_eq!(i as Distance, d, "incoherent distance returned for i");
        }
    }

    #[test]
    fn test_with_one_big_edge() {
        let path_lengths: Vec<Distance> = vec![
            0,
            1 * 4,
            2 * 4,
            3 * 4,
            4 * 4,
            5 * 4,
            6 * 4,
            7 * 4,
            8 * 4,
            9 * 4,
        ];
        let predecessor: Vec<Option<Node>> =
            Vec::from_iter((0..10).map(|i| if i == 0 { None } else { Some(i - 1) }));
        let rhos: Rhos = (0..10).map(|i| i as Rho).collect();

        let result = min_rho_along_path(&path_lengths, &predecessor, &rhos, 0, 9, 17, 19);
        assert_eq!(result, (4, 4 * 4));
    }
}
