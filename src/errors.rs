//! Error handling for rhopsets.
//!
//! There is one main enum for errors, with From<T> implementations when the usage was needed.

use crate::graph::Node;
use petgraph::algo::NegativeCycle;
use std::error::Error;
use std::fmt;
use std::fmt::{Debug, Display, Formatter};
use std::io;
use std::num;

/// Main type of errors in the projects.
#[derive(Debug)]
pub enum ErrorKind {
    NotConnected(Node, Node),
    NegativeCycle(petgraph::algo::NegativeCycle),
    FileIOError(io::Error),
    ParseIntError(num::ParseIntError),
    ParseFloatError(num::ParseFloatError),
    ParseGraphTypeError(String),
}

impl Error for ErrorKind {}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            ErrorKind::NotConnected(u, v) => write!(f, "Not Connected : {} and {}.", u, v),
            ErrorKind::NegativeCycle(e) => e.fmt(f),
            ErrorKind::FileIOError(e) => Display::fmt(&e, f),
            ErrorKind::ParseIntError(e) => Display::fmt(&e, f),
            ErrorKind::ParseFloatError(e) => Display::fmt(&e, f),
            ErrorKind::ParseGraphTypeError(e) => Display::fmt(&e, f),
        }
    }
}

impl From<petgraph::algo::NegativeCycle> for ErrorKind {
    fn from(e: NegativeCycle) -> Self {
        return ErrorKind::NegativeCycle(e);
    }
}

impl From<io::Error> for ErrorKind {
    fn from(e: io::Error) -> Self {
        return ErrorKind::FileIOError(e);
    }
}

impl From<num::ParseFloatError> for ErrorKind {
    fn from(e: num::ParseFloatError) -> Self {
        return ErrorKind::ParseFloatError(e);
    }
}

impl From<num::ParseIntError> for ErrorKind {
    fn from(e: num::ParseIntError) -> Self {
        return ErrorKind::ParseIntError(e);
    }
}
