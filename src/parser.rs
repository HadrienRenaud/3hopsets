//! Utils to import or export a graph to a file
//!
//! All the functions in this module are pretty efficient, in a sense that using serde would not
//! give a significant performance improvement.
//!
//! They use `itoa::write` to write a integers to a buffer, which is what is used internally by the
//! standard librairy to display integers. But by doing this, they skip all the complex machinery
//! of `fmt!`.

use std::fs::File;

use crate::errors::ErrorKind;
use crate::graph::{Distance, Edge, Graph, Node};
use std::io::{stdout, BufRead, BufReader, BufWriter, Write};

/// Parse a str into a Node
#[inline]
fn parse_item_into_node(item: Option<&str>) -> Result<Node, ErrorKind> {
    item.ok_or(ErrorKind::ParseGraphTypeError(String::from(
        "Cannot read u",
    )))?
    .parse::<Node>()
    .map_err(ErrorKind::ParseIntError)
}

/// Parse a graph line into an edge
#[inline]
fn parse_graph_line(line: &str) -> Result<Edge, ErrorKind> {
    let mut iter = line.split_ascii_whitespace();
    let u = parse_item_into_node(iter.next())?;
    let v = parse_item_into_node(iter.next())?;
    let w = parse_item_into_node(iter.next())?;
    return Ok((u, v, w as Distance));
}

/// Parse a graph from a file into a graph
///
/// Example:
/// ```
/// use rhopsets::parser::parse_graph;
/// let graph = parse_graph("./assets/very_small.txt").unwrap();
/// println!("{}", graph); // Graph(n=5, m=7)
/// ```
pub fn parse_graph(filename: &str) -> Result<Graph, ErrorKind> {
    let reader = BufReader::new(File::open(filename)?);
    let mut graph = Graph::new();

    for r_line in reader.lines() {
        let line = r_line?;
        let (u, v, w) = parse_graph_line(line.as_str())?;
        graph.add_edge(u, v, w);
    }

    Ok(graph)
}

/// Write an edge to the buffer buf
/// It uses the syntax `u v w` where the edge is (u, v) of weight w.
#[inline]
fn write_graph_line<T>((u, v, w): Edge, mut buf: T) -> Result<(), ErrorKind>
where
    T: Write,
{
    let space: &[u8] = " ".as_ref();
    itoa::write(&mut buf, u)?;
    buf.write(space)?;
    itoa::write(&mut buf, v)?;
    buf.write(space)?;
    itoa::write(&mut buf, w as u64)?;
    buf.write("\n".as_ref())?;
    Ok(())
}

/// Write graph into filename
///
/// Each line of the created file corresponds to an edge (u, v) of weight w with the syntax `u v w`.
///
/// Example:
/// ```
/// # use rhopsets::parser::export_graph;
/// # use rhopsets::graph::generate_circle;
///
/// let graph = generate_circle(4);
/// export_graph(&graph, "./assets/Circle-4.txt").expect("");
/// // Now ./assets/Circle-4.txt contains
/// // 1 2 1
/// // 0 1 1
/// // 0 3 1
/// // 2 3 1
/// ```
pub fn export_graph(graph: &Graph, filename: &str) -> Result<(), ErrorKind> {
    let mut writer = BufWriter::new(File::create(filename)?);

    for edge in graph.all_edges() {
        write_graph_line(edge, &mut writer)?;
    }

    Ok(())
}

#[derive(PartialEq, Debug)]
enum GraphNumber {
    One = 1,
    Two = 2,
}

/// Parse an hopset line into an edge and a graph number
#[inline]
fn parse_hopsets_line(line: &str) -> Result<(Edge, GraphNumber), ErrorKind> {
    let mut iter = line.split_ascii_whitespace();
    let u = parse_item_into_node(iter.next())?;
    let v = parse_item_into_node(iter.next())?;
    let w = parse_item_into_node(iter.next())?;

    let edge = (u, v, w as Distance);

    match iter
        .next()
        .ok_or(ErrorKind::ParseGraphTypeError(String::from(
            "Cannot read graph number",
        )))? {
        "1" => Ok((edge, GraphNumber::One)),
        "2" => Ok((edge, GraphNumber::Two)),
        _ => Err(ErrorKind::ParseGraphTypeError(String::from(
            "Wrong graph number",
        ))),
    }
}

const MIN_GRAPH_NODE_CAPACITY: usize = 8192; // = 2 ** 13
const MIN_GRAPH_EDGE_CAPACITY: usize = MIN_GRAPH_NODE_CAPACITY * 3 / 2;

/// Parse hopsets written as in `export_hopsets_stdout` into 2 graphs
///
/// Example:
/// ```bash
/// >>> cat ./assets/3hopsets.txt
/// 0 1 1 1
/// 1 2 1 1
/// 2 3 1 1
/// 3 4 1 1
/// 4 5 1 1
/// 1 4 3 2
/// ```
/// ```
/// # use rhopsets::parser::parse_hopsets_file;
/// let (g1, g2) = parse_hopsets_file("./assets/3hopsets.txt").expect("Cannot read file");
/// println!("Found g1={}", g1);
/// // Found g1=Graph(n=6, m=5)
/// println!("Found g2={}", g2);
/// // Found g2=Graph(n=5, m=1);
/// ```
pub fn parse_hopsets_file(filename: &str) -> Result<(Graph, Graph), ErrorKind> {
    let reader = BufReader::new(File::open(filename)?);

    // Here we pre-alloc some space, in order to reduce the need to extend the graph capacity.
    // It should not impact too much small graphs
    let mut g1 = Graph::with_capacity(MIN_GRAPH_NODE_CAPACITY, MIN_GRAPH_EDGE_CAPACITY);
    let mut g2 = Graph::with_capacity(MIN_GRAPH_NODE_CAPACITY, MIN_GRAPH_EDGE_CAPACITY);

    for r_line in reader.lines() {
        let line = r_line?;
        let ((u, v, w), i) = parse_hopsets_line(line.as_str())?;
        match i {
            GraphNumber::One => g1.add_edge(u, v, w as Distance),
            GraphNumber::Two => g2.add_edge(u, v, w as Distance),
        }
    }

    Ok((g1, g2))
}

/// Write an edge with graph number (1 or 2) to buf
#[inline]
fn write_hopsets_line<T>(
    (u, v, w): Edge,
    graph_number: GraphNumber,
    mut buf: T,
) -> Result<(), ErrorKind>
where
    T: Write,
{
    let space: &[u8] = " ".as_ref();
    // itoa::write is the same code use internally by write!(buf, "{}", u) but
    // without the additional machinery
    itoa::write(&mut buf, u)?;
    buf.write(space)?;
    itoa::write(&mut buf, v)?;
    buf.write(space)?;
    itoa::write(&mut buf, w as u64)?; // With float, we would have to use ftoa
    match graph_number {
        GraphNumber::One => buf.write(" 1\n".as_ref())?,
        GraphNumber::Two => buf.write(" 2\n".as_ref())?,
    };

    Ok(())
}

/// Write graphs into stdout
///
/// Each printed line corresponds to an edge (u, v) of weight w in graph g_i with the syntax `u v w i`.
///
/// Example:
/// ```
/// # use rhopsets::parser::{export_graph, export_hopsets_stdout};
/// # use rhopsets::graph::{generate_circle, Graph};
///
/// let g1 = generate_circle(4);
/// let g2 = Graph::from_edges(vec![(1, 2, 1)].into_iter());
/// export_hopsets_stdout(&g1, &g2).expect("");
/// // 1 2 1 1
/// // 0 1 1 1
/// // 0 3 1 1
/// // 2 3 1 1
/// // 2 3 1 2
/// ```
pub fn export_hopsets_stdout(g1: &Graph, g2: &Graph) -> Result<(), ErrorKind> {
    let mut writer = BufWriter::new(stdout());

    log::info!("Exporting graph g1 ={} ...", g1);
    for e in g1.all_edges() {
        write_hopsets_line(e, GraphNumber::One, &mut writer)?;
    }

    log::info!("Exporting graph g2 ={} ...", g2);
    for e in g2.all_edges() {
        write_hopsets_line(e, GraphNumber::Two, &mut writer)?;
    }

    log::info!("Exported graphs.");

    return Ok(());
}

/// Print edges1 and edges2 to stdout
pub fn export_hopsets_edges<I>(edges1: I, edges2: I) -> Result<(), ErrorKind>
where
    I: Iterator<Item = Edge> + std::marker::Send,
{
    // We parallelize the parsing of edges1 and edges2 by writing first edges2 to a buf
    // writer is a buffered stdout
    let mut writer = BufWriter::new(stdout());
    // buf is a vec, initialized with enough capacity to hold the whole export of edges2
    // we first get a hint of edges2, and then multiply it by the maximum length of an
    // exported edge, ie 65 = 3 u64 + 3 spaces + 1 or 2 + \n
    let (low, high) = edges2.size_hint();
    let mut buf = Vec::<u8>::with_capacity(high.unwrap_or(low) * 65);

    // At the same time, we print edges of edges1 to stdout, and edges of edges2 to buf
    let results = rayon::join(
        || -> Result<(), ErrorKind> {
            for e in edges1 {
                write_hopsets_line(e, GraphNumber::One, &mut writer)?;
            }
            Ok(())
        },
        || -> Result<(), ErrorKind> {
            for e in edges2 {
                write_hopsets_line(e, GraphNumber::Two, &mut buf)?;
            }
            Ok(())
        },
    );

    // We ensure that these possesses did go well
    results.0?;
    results.1?;

    // Then copy buf to stdout
    writer.write_all(buf.as_ref())?;

    // Ensure everything is well printed
    writer.flush()?;

    return Ok(());
}

#[inline]
fn parse_query(line: &str) -> Result<(Node, Node), ErrorKind> {
    let mut iter = line.split_ascii_whitespace();
    let u = parse_item_into_node(iter.next())?;
    let v = parse_item_into_node(iter.next())?;
    Ok((u, v))
}

pub fn parse_queries(filename: &str) -> Result<Vec<(Node, Node)>, ErrorKind> {
    let reader = BufReader::new(File::open(filename)?);

    let mut queries = Vec::<(Node, Node)>::new();
    for line in reader.lines() {
        queries.push(parse_query(line?.as_str())?);
    }

    return Ok(queries);
}

pub fn write_query_result<T>(d: Distance, mut buf: T) -> Result<(), ErrorKind>
where
    T: Write,
{
    itoa::write(&mut buf, d as u64)?;
    buf.write("\n".as_ref())?;
    Ok(())
}
