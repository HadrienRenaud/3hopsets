//! An implementation of a graph structure

use petgraph::graphmap::UnGraphMap;
use std::cmp::max;
use std::collections::hash_map::{HashMap, Iter as HashMapIter};
use std::iter::repeat;
use std::ops::Range;
use std::{fmt, slice};

pub type Node = usize;
pub type Distance = u64;
pub type Edge = (Node, Node, Distance);

type EdgeKey = (Node, Node);

/// A simple but efficient graph structure
///
/// Lookup is usually in O(1), for node and edges.
/// Iterations on neighbors is also without overhead.
/// The only operation a little bit costly is `remove_edge` which is used only for tests.
///
/// This relies both on a LinkedList of neighbors and a HashTable of edges.
#[derive(Clone, Debug)]
pub struct Graph {
    neighbors: Vec<Vec<(Node, Distance)>>,
    edges: HashMap<(Node, Node), Distance>,
}

impl Default for Graph {
    fn default() -> Self {
        Graph::new()
    }
}

impl Graph {
    /// A new empty
    pub fn new() -> Self {
        Self::with_capacity(0, 0)
    }

    /// A new with capacity
    pub fn with_capacity(nodes: usize, edges: usize) -> Self {
        Graph {
            neighbors: Vec::with_capacity(nodes),
            edges: HashMap::with_capacity(edges),
        }
    }

    /// Returns the number of nodes in the graph
    #[inline]
    pub fn node_count(&self) -> usize {
        self.neighbors.len()
    }

    /// Returns the number of edges in the graph
    #[inline]
    pub fn edge_count(&self) -> usize {
        self.edges.len()
    }

    /// This function build an edge key from an edge pair.
    #[inline]
    fn edge_key(u: Node, v: Node) -> EdgeKey {
        if u < v {
            (u, v)
        } else {
            (v, u)
        }
    }

    const DEFAULT_NEIGHBORS_CAPACITY: usize = 6;

    pub fn add_edge(&mut self, u: Node, v: Node, w: Distance) {
        let key = Self::edge_key(u, v);

        // Ensure there is enough place in neighbors array
        let maxi = max(u, v);
        if self.neighbors.len() <= maxi {
            let to_extend = (maxi as i64 + 1 - self.neighbors.len() as i64) as usize;
            self.neighbors.reserve(to_extend);
            self.neighbors.extend(
                repeat(Vec::<(Node, Distance)>::with_capacity(
                    Self::DEFAULT_NEIGHBORS_CAPACITY,
                ))
                .take(to_extend),
            );
        }

        // Update neighbors
        // ie key was not in edges before
        if let None = self.edges.insert(key, w) {
            if let Some(vec) = self.neighbors.get_mut(u) {
                vec.push((v, w));
            }
            if let Some(vec) = self.neighbors.get_mut(v) {
                vec.push((u, w));
            }
        }
    }

    pub fn from_edges<I>(iter: I) -> Self
    where
        I: Iterator<Item = Edge>,
    {
        let (low_size, _) = iter.size_hint();
        return Self::from_edges_with_capacity(0, low_size, iter);
    }

    pub fn from_edges_with_capacity<I>(n: usize, m: usize, iter: I) -> Self
    where
        I: Iterator<Item = Edge>,
    {
        let mut g = Self::with_capacity(n, m);

        for (u, v, w) in iter {
            g.add_edge(u, v, w);
        }

        return g;
    }

    pub fn contains_edge(&self, u: Node, v: Node) -> bool {
        self.edges.contains_key(&Self::edge_key(u, v))
    }

    pub fn edge_weight(&self, u: Node, v: Node) -> Option<Distance> {
        self.edges.get(&Self::edge_key(u, v)).copied()
    }

    pub fn all_edges(&self) -> AllEdges {
        AllEdges {
            inner: self.edges.iter(),
        }
    }

    pub fn nodes(&self) -> Range<Node> {
        0..self.node_count()
    }

    pub fn edges(&self, u: Node) -> EdgesFrom {
        EdgesFrom {
            from: u,
            inner: self.neighbors[u].iter(),
        }
    }

    pub fn neighbors(&self, u: Node) -> Neighbors {
        Neighbors {
            inner: self.neighbors[u].iter(),
        }
    }

    #[inline]
    fn remove_from_neighbors(vec: &mut Vec<(Node, Distance)>, item: Node) -> bool {
        let index = vec.iter().position(|(u, _)| *u == item);
        match index {
            None => false,
            Some(i) => {
                vec.remove(i);
                true
            }
        }
    }

    pub fn remove_edge(&mut self, u: Node, v: Node) -> bool {
        let key = Self::edge_key(u, v);
        if self.edges.contains_key(&key) {
            self.edges.remove(&key).is_some()
                & Self::remove_from_neighbors(self.neighbors.get_mut(u).unwrap(), v)
                & Self::remove_from_neighbors(self.neighbors.get_mut(v).unwrap(), u)
        } else {
            false
        }
    }
}

impl fmt::Display for Graph {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Graph(n={}, m={})", self.node_count(), self.edge_count())
    }
}

pub struct AllEdges<'a> {
    inner: HashMapIter<'a, (Node, Node), Distance>,
}

impl<'a> Iterator for AllEdges<'a> {
    type Item = Edge;

    fn next(&mut self) -> Option<Self::Item> {
        match self.inner.next() {
            Some(((u, v), &w)) => Some((*u, *v, w)),
            None => None,
        }
    }
}

pub struct EdgesFrom<'a> {
    from: Node,
    inner: slice::Iter<'a, (Node, Distance)>,
}

impl<'a> Iterator for EdgesFrom<'a> {
    type Item = Edge;

    fn next(&mut self) -> Option<Self::Item> {
        match self.inner.next() {
            None => None,
            Some((v, w)) => Some((self.from, *v, *w)),
        }
    }
}

pub struct Neighbors<'a> {
    inner: slice::Iter<'a, (Node, Distance)>,
}

impl<'a> Iterator for Neighbors<'a> {
    type Item = Node;

    fn next(&mut self) -> Option<Self::Item> {
        match self.inner.next() {
            None => None,
            Some(&(u, _)) => Some(u),
        }
    }
}

/// Generate a cyclic graph of dimension n
pub fn generate_circle(n: usize) -> Graph {
    let mut g = Graph::with_capacity(n, n);
    for i in 0..n {
        g.add_edge(i, (i + 1) % n, 1);
    }
    return g;
}

/// Generate a cyclic graph of dimensions (n, n)
pub fn generate_grid(n: usize) -> Graph {
    let n2 = n * n;
    let mut graph = Graph::with_capacity(n2 as usize, 4 * n2 as usize);

    let coord = |i: Node, j: Node| i * n + j;

    for i in 0..n {
        for j in 0..n {
            graph.add_edge(coord((i + 1) % n, j), coord(i, j), 1);
            graph.add_edge(coord((i + n - 1) % n, j), coord(i, j), 1);
            graph.add_edge(coord(i, j), coord(i, (j + 1) % n), 1);
            graph.add_edge(coord(i, j), coord(i, (j + n - 1) % n), 1);
        }
    }

    return graph;
}

/// Export a graph to petgraph::Graph. Useful in tests
pub fn to_graphmap(graph: &Graph) -> UnGraphMap<Node, f64> {
    let mut g = UnGraphMap::with_capacity(graph.node_count(), graph.edge_count());

    for u in graph.nodes() {
        g.add_node(u);
    }
    for (u, v, w) in graph.all_edges() {
        g.add_edge(u, v, w as f64);
    }

    return g;
}
