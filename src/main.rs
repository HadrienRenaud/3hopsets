//! Uses the rhopsets library to provide cli usage

#[macro_use]
extern crate clap;

use clap::{App, AppSettings, Arg, ArgSettings, SubCommand};
use env_logger;

use rhopsets::builder::build_three_hopsets;
use rhopsets::graph::{generate_circle, generate_grid, Node};
use rhopsets::oracle::hopsets_oracle;
use rhopsets::parser::{
    export_graph, parse_graph, parse_hopsets_file, parse_queries, write_query_result,
};
use std::fs::File;
use std::io::{stdout, BufWriter};

fn int_validator(arg: String) -> Result<(), String> {
    arg.parse::<u32>()
        .map(|_| ())
        .map_err(|_| "this should be an integer.".to_string())
}

fn float_validator(arg: String) -> Result<(), String> {
    arg.parse::<f64>()
        .map(|_| ())
        .map_err(|_| "this should be a floating point number.".to_string())
}

fn file_exists_validator(arg: String) -> Result<(), String> {
    match File::open(arg) {
        Ok(_) => Ok(()),
        Err(e) => Err(format!("Cannot open file: {}", e)),
    }
}

fn file_writable_validator(arg: String) -> Result<(), String> {
    match File::create(arg) {
        Ok(_) => Ok(()),
        Err(e) => Err(format!("Cannot write to file: {}", e)),
    }
}

arg_enum! {
    enum GraphTypeArg {
        Circle,
        Grid,
    }
}

fn main() {
    let matches = App::new("3hopsets")
        .author("Hadrien Renaud")
        .about("Build and use 3-hop labels in a graph.")
        .setting(AppSettings::SubcommandRequired)
        .setting(AppSettings::VersionlessSubcommands)
        .setting(AppSettings::DisableVersion)
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .subcommand(
            SubCommand::with_name("build")
                .about("Build a 3-hop labelling for the graph.")
                .arg(Arg::with_name("D'").required(true).validator(float_validator)
                    .help("The minimal distance from which we pass from 1 hop to 2 hop labellling."))
                .arg(Arg::with_name("EPSILON").required(true).validator(float_validator)
                    .help("The increasing factor for d rounds."))
                .arg(Arg::with_name("GRAPH")
                    .help("The file containing the graph. Each line of this file should be of the format `u v w` where (u, v) is an edge of weight w in the graph.")
                    .validator(file_exists_validator).required(true))
        )
        .subcommand(
            SubCommand::with_name("distorac")
                .about("Compute the graph distance for each query.")
                .arg(Arg::with_name("HOPSETS_FILE").required(true).validator(file_exists_validator)
                    .help("The lines of this file should be of the format `u v w i` where (u, v) is an edge of weight w in the graph i."))
                .arg(Arg::with_name("QUERIES_FILE").required(true).validator(file_exists_validator)
                    .help("The lines of this file should be of the format `u v` where u and v are two nodes in the graph passed as argument.")),
        )
        .subcommand(
            SubCommand::with_name("generate")
                .about("Generate a graph and write it to a file.")
                .arg(Arg::with_name("TYPE").required(true).possible_values(&GraphTypeArg::variants())
                    .help("The topology of the graph.").set(ArgSettings::CaseInsensitive))
                .arg(Arg::with_name("N").required(true).validator(int_validator)
                    .help("The size of the graph."))
                .arg(Arg::with_name("FILE").validator(file_writable_validator).takes_value(true)
                    .short("e").long("export").help("The file to export to. Default to `./assets/<TYPE>-<N>.txt`."))
        )
        .get_matches();

    let mut builder = env_logger::builder();
    match matches.occurrences_of("verbose") {
        0 => {}
        1 => {
            builder.filter_level(log::LevelFilter::Info);
        }
        _ => {
            builder.filter_level(log::LevelFilter::Debug);
        }
    }
    builder.init();

    match matches.subcommand() {
        ("build", Some(submatches)) => {
            let filename = submatches
                .value_of("GRAPH")
                .expect("You need the graph argument");
            let rgraph = parse_graph(filename);
            if rgraph.is_err() {
                eprintln!("Parsing error: {}", rgraph.unwrap_err());
                return;
            }
            let graph = rgraph.unwrap();
            let d_ = value_t!(submatches, "D'", f64).unwrap();
            let epsilon = value_t!(submatches, "EPSILON", f64).unwrap();

            build_three_hopsets(&graph, d_, epsilon).expect("Cannot build hopsets");
        }
        ("distorac", Some(submatches)) => {
            let hopsets_file = submatches.value_of("HOPSETS_FILE").unwrap();
            let queries_file = submatches.value_of("QUERIES_FILE").unwrap();

            let (g1, g2) = parse_hopsets_file(hopsets_file).expect("Cannot parse graph g1");
            let mut out = BufWriter::new(stdout());
            for query in parse_queries(queries_file).expect("Cannot read queries file") {
                write_query_result(hopsets_oracle(&g1, &g2, query.0, query.1), &mut out)
                    .expect("Cannot write to stdout");
            }
        }
        ("generate", Some(submatches)) => {
            let t = value_t!(submatches, "TYPE", GraphTypeArg).unwrap();
            let n = value_t!(submatches, "N", Node).unwrap();
            let default_filename = format!("./assets/{}-{}.txt", t, n);
            let filename = submatches
                .value_of("FILE")
                .unwrap_or(default_filename.as_str());

            let graph = match t {
                GraphTypeArg::Circle => generate_circle(n),
                GraphTypeArg::Grid => generate_grid(n),
            };

            export_graph(&graph, filename).expect("Could not write to file");
        }
        (&_, _) => println!("One subcommand is needed."),
    };
}
