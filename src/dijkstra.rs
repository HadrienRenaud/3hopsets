//! Dijkstra algorithm for computing all-pairs shortest paths from a node.
//!
//! We first implement ordering for pairs (Distance, Node) so these items can be put in a binary
//! heap that will store the ordered elements to be parsed.
//!
//! Then it is the `dijkstra` function, which is a classic Dijkstra algorithm, with a few
//! specificities:
//! - We store paths from the starting point u to every node as a LinkedList: for every node, we
//!     store its neighbor closest to u ;
//! - We compute the shortest path for all pairs of nodes ;
//! - We don't have to consider strange edge cases, so our algorithm is fairly simple
//!
//! Everything here is adapted from petgraph source.

use crate::graph::{Distance, Graph, Node};

use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashSet};

const ZERO_DISTANCE: Distance = 0 as Distance;

/// A simple tuple with ordering
#[derive(Copy, Clone, Debug)]
struct NodeWithDistance(Distance, Node);

impl PartialEq for NodeWithDistance {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl Eq for NodeWithDistance {}

impl PartialOrd for NodeWithDistance {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for NodeWithDistance {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        // Note that the order is inverse
        other.0.cmp(&self.0)
    }
}

/// An implementation of Dijkstra algorithm
///
/// Returns:
///  - A Vec of distances to the starting node
///  - A Vec of predecessors along a shortest path. It is defined for every node different from
///     start but connected to it.
///
/// ```
/// # use rhopsets::graph::Graph;
/// # use rhopsets::dijkstra::dijkstra;
///
/// let mut graph = Graph::from_edges(vec![
///     (0, 1, 1),
///     (1, 2, 1),
///     (2, 3, 1),
///     (3, 4, 1),
///     (4, 5, 1),
/// ].into_iter());
///
/// let (distances, predecessors) = dijkstra(&graph, 0);
///
/// assert_eq!(distances, vec![0, 1, 2, 3, 4, 5]);
/// assert_eq!(predecessors, vec![None, Some(0), Some(1), Some(2), Some(3), Some(4)])
/// ```
pub fn dijkstra(graph: &Graph, start: Node) -> (Vec<Distance>, Vec<Option<Node>>) {
    let mut visited = HashSet::<Node>::new();
    let mut distances = vec![ZERO_DISTANCE; graph.node_count()];
    let mut predecessor = vec![None; graph.node_count()];

    let mut visit_next = BinaryHeap::<NodeWithDistance>::with_capacity(graph.node_count());

    visit_next.push(NodeWithDistance(Distance::default(), start));

    while let Some(NodeWithDistance(d_u, u)) = visit_next.pop() {
        if visited.contains(&u) {
            continue;
        }

        for (_, v, w) in graph.edges(u) {
            if visited.contains(&v) {
                continue;
            }
            let d_v = d_u + w;
            if predecessor[v as usize].is_none() || d_v < distances[v as usize] {
                distances[v as usize] = d_v;
                visit_next.push(NodeWithDistance(d_v, v));
                predecessor[v as usize] = Some(u);
            }
        }

        visited.insert(u);
    }

    return (distances, predecessor);
}

#[cfg(test)]
mod tests {
    use crate::dijkstra::dijkstra;
    use crate::graph::{generate_circle, generate_grid, to_graphmap, Distance, Graph, Node};
    use petgraph::algo::bellman_ford;

    #[test]
    fn test_on_simple_path() {
        let n = 10;
        let mut graph = generate_circle(n);
        graph.remove_edge(0, n - 1);

        let (ds, ps) = dijkstra(&graph, 0);
        assert_eq!(ps[0], None);
        for i in 0..(n as usize) {
            assert_eq!(ds[i], i as Distance, "Wrong distance found for {}", i);
            if i > 0 {
                assert_eq!(
                    ps[i],
                    Some((i - 1) as Node),
                    "Wrong predecessor found for {}",
                    i
                );
            }
        }
    }

    fn _against_bellman_ford(graph: &Graph, u: Node, unique: bool) {
        let (ds, ps) = dijkstra(graph, u);
        let (expected_ds, expected_ps) =
            bellman_ford(&to_graphmap(graph), u).expect("Should not fail");
        for i in 0..(graph.node_count() as usize) {
            assert_eq!(
                ds[i] as f64, expected_ds[i],
                "Wrong distance found for {}",
                i
            );
            // If there is a unique path from u to v, then we can test for predecessors.
            if unique {
                assert_eq!(ps[i], expected_ps[i], "Wrong predecessor found for {}", i);
            }
        }
    }

    #[test]
    fn test_on_circle() {
        let n = 100;
        let graph = generate_circle(n);

        _against_bellman_ford(&graph, 0, false);
        _against_bellman_ford(&graph, n / 2, false);
    }

    #[test]
    fn test_on_grid() {
        let n = 10;
        let graph = generate_grid(n);

        _against_bellman_ford(&graph, 0, false);
        _against_bellman_ford(&graph, n / 2, false);
    }
}
