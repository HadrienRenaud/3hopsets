# 3 hopsets

Project for [MPRI 2.29.2 - Graph Mining](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-29-2).

> The goal of this subject is to implement a distance oracle based on a 3-hop labelling for a given undirected weighted graph G with unique shortest paths.

Useful links 

- [The subject](./3hopset.pdf)
- [Installation](#installation)
- [Report](./report.pdf)


## Installation

### Build from source

With cargo, clone the project and then run inside it :

```bash
cargo build --release
```

You can now access the cli from `target/release/rhopsets`.

## Usage

_In every case, `--help` is a good solution._

The main binary is located in `target/release/rhopsets`.

## Design

The first version of this tool has been written in Python.
It worked, but was very very slow : on `assets/corse.txt`, it took a couple of days to run.

This is a rebuild of the first tool in Rust.
It relies on [`petgraph`](https://docs.rs/petgraph/0.5.1/petgraph/index.html) for the Graph structure, and has provided first implementations of Bellman Ford that was used. 
For performance reasons, I decided to adapt their Dijkstra algorithm to suit my needs (no polymorphism, but some way to rebuild the paths afterwards).

For parallelism, I use the brilliant [rayon](https://docs.rs/rayon/1.5.0/rayon/index.html), which provide a particularly simple way to configure a parallel iterator that spawn the right number of system threads. That provided me a x7 speedup on my 8-core machine for a 100x100 grid.

For a more detailed description, see the [report](./report.pdf).

## Testing

As rust has an integrated framework for unit testing, I use it for critical functions, such as the Dijkstra Algorithm or the function that computes the node with the minimal rho on a path.

### Integration tests

The first version of this tool built the complete graphs before printing it. It allowed me to test the generated graphs.
The procedure was roughly as follows:
```python3
def _test_on_graph(graph: Graph, d, epsilon):
    g1, g2 = build_hopsets(graph, d, epsilon)
    for u in graph.nodes():
        for v in graph.nodes():
            self.assertAlmostEqual(dijkstra_oracle(graph, u, v), hopsets_oracle(g1, g2, u, v))
```

Here, as I wanted to increase performance, I had to print eagerly every found edge. Testing this was strange and did not fit well into `cargo test`'s workflow.

I ended up doing both, by letting a `const` boolean control if rebuilding the graph is needed or printing it.

This is used for integrations tests in `tests/integration.rs`, and I built a similar function in Rust for extensively test all the parts.
