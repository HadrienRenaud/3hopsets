---
title: Distance Oracles Based on 3-Hopsets
subtitle: Project for [MPRI 2.29.2 - Graph Mining](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-29-2)
author: Hadrien Renaud
date: 15 nov. 2020
documentclass: article
colorlinks: true
header-includes: |
    \usepackage{fullpage}
---

This report is the answer to [this subject](./3hopset.pdf). 
It comes with the code which is available [here](https://gitlab.com/HadrienRenaud/3hopsets).
All timings here are done on my 8-core intel-i7 laptop, with rust `1.45.0`.

This report is structured chronologically, as I believe to have followed a path from the most simple implementation 
to more complex optimizations.
The [first part](#python) relates my first try in Python, [the second](#a-more-successful-attempt-in-rust) how I made it faster in Rust. 

# A first try in Python {#python}

_If the final implementation that comes with this report is in Rust, I wanted to talk about my first try in Python 
because the process is quite similar for both implementations._

## Correctness

My first goal for this project was to build a first very simple tool that was **correct**.
For that, I allowed myself to do it in the language that I understand the best, Python3, and to use any librairy that 
provided graph algorithms.
I did a little survey of Python librairies and found [`python-igraph`](https://igraph.org/python/) that provides a 
Python compatibility layer over a C.
It was [fairly performant](https://graph-tool.skewed.de/performance) and quite simple to use. 

What followed is that I implemented a first version of this tool.
I needed to assure that my code was behaving the right way to ensure correctness.
In Python, that means to use tests, both unit tests and integration tests.
Unit tests allowed me to debug my `get_min_rho_along_path` function, that given a shortest path from u to v,
and returned the node along the path with the lower rho that respect distance contraints.

```python
def min_rho_along_path(
    graph: Graph, path: List[Vertex], d_min: float, d_max: float
) -> Tuple[VertexID, EdgeWeight]:
    """Return x minimizing rho along the path (u, ... v)
        where the distance ux is in [d_min, d_max]."""
    # ... iterate over the path, thus computing the minimum in the right section ...
```

This function is a bit tricky to implement, because it has a big number of edge cases.

#### Integration tests
were interesting to debug the complete hopset construction algorithm.
Given an input graph, I generated the two graphs, and then compared the result of my distance oracle to the Dijkstra 
implementation which is builtin in igraph, on every pair of nodes in the graph.
I did that for a couple of graphs that could be generated with `igraph`.
 
## Performance
This process gave me a pretty reliable tool, but unfortunately, its performance was very bad.
Based on simple extrapolation, it would need around 3 days to compute the hotsets on `corse.txt`.
To improve the speed of a python program, I only know 4 ways:

- improve the algorithm: the algorithm is given by the subject, so progress along that axis is limited [^1] ;
- use multiple cores at once (ie using [`multiprocessing`](https://docs.python.org/3.8/library/multiprocessing.html)) ;
- use a python compiler, or just-in-time compiler ;
- implement the algorithm in a more low-level language, or use a librairy that does that for you, but I don't know how 
    to do that.

[^1]: In fact, as the graph is undirected in the subject, and not in the article, we can only treat half of the paths.
    I the Rust implementation, I choosed to only treat paths $(u, v)$ for which $u < v$.

I successfully implemented parallelism with `multiprocessing` with a `ThreadPool`.
However, the usage of [`numba`](https://numba.pydata.org/) was far more complicated: for each function either I could not infer
its correct usage, either it did not provide enough performance improvement.
If my searches are correct, it is the only Python-JIT that can be installed with pip. 

At this point, my profiler tool told me that most of the time spent in my function was to rebuild the paths from 
`igraph`'s output.
This meant that I had to customize the shortest path algorithm in order to get the right output that did not need 
further computation in Python.

Having unsuccessfully attempted to implement this in Python, I decided to try it in Rust. 
There are multiple reasons for this:

- I wanted a very low-level compiled language, in order to be able to implement the correct performance improvements 
    that I wanted to do ; 
- I already have a little experience in Rust, and it provides some safety that pleases me ;
- I had not managed a alone C++ or ocaml project, and my experience with C++ memory management was not very pleasant ;
- I knew that [`cross`](https://github.com/rust-embedded/cross) provides a very simple cross compiling platform for rust.

# A more successful attempt in Rust

As in my first attempt in Python, my first concern was about correctness.
In a second time, I tried to optimize it.

## Towards a correct implementation

As the number of libraries for graphs in Rust is smaller, I chose [`petgraph`](https://docs.rs/petgraph/0.5.1/petgraph/)
which has a better documentation and is more used than its concurrents.
The `petgraph` crate comes with a graph implementation that is based on `HashMap`s for nodes indexed directly with 
integers, which is our case.
Its spatial complexity is $\mathcal{O}(E)$, and lookup time for members, edges, and neighbors are $\mathcal{O}(1)$.
It provides also an implementation of bellman-ford that can be used to compute the shortest path from u to every node 
in the graph, which can be useful if any of these things .

With this crate, I rebuilt an implementation of this tool. 
Adapting my code from Python was fairly easy, and I needed far less iterations on my unit tests to be sure that my
function were behaving properly.
I also re-implemented integration tests to make sure the complete workflow worked well.

As the graph librairy in Rust comes with less features than `igraph`, I needed to rebuild some features like graph
generation.
However, it was already far faster than its python counterpart, it took only a few hours to build the hopsets on
`corse.txt`.

## Make it faster

In order to improve, the performance of my implementation, I used different techniques.

#### I used [`rayon`](https://docs.rs/rayon/1.5.0/rayon/) to parallelize the main loop of my code. {#rayon}
That provided me a 7x speed-up.
The main loop now looks like this:
```rust
fn build_three_hopsets(graph: &Graph, d_: f64, epsilon: f64) {
    let rhos = generate_random_array(graph.node_count());
    let produced_edges = graph.nodes()  // iterate on nodes
                              .par_iter()  // Make it a parallel iterator 
                              .map(|node| build_from_node(u, graph, rhos, d_, epsilon));
    // Post process the edges
}
```
However, I only use the most simple `ParallelIterator`, I could reduce the number of copies between processes by 
implementing a custom `ThreadPool` that would be initiated with the input graph.
I did not have the time to do that, and the added complexity of the code would be substantial.

#### Replace `petgraph`
I replaced [`petgraph::algo::bellman_ford`](https://docs.rs/petgraph/0.5.1/petgraph/algo/fn.bellman_ford.html) by a custom implementation of Dijkstra.
That allowed me to have implemented 100% of the algorithm, except for the underlying data-structures.
That also gave me a significant speed up[^2], because I could swap some `HashMap` for `Vec` because I did not need
any polymorphism, and because my nodes are indexed by 64 bits integers.
From what I read in different papers, an iterated Dijkstra is in practice quite performant on very sparse graphs such as `corse.txt`.

[^2]: Not measured, sorry.

I also re-implemented the `Graph` structure that I used, in order to use custom and optimized buffered.

#### I buffered the print-to-`stdout` process, and implemented a custom serializer. {#buffered-stdout}
Instead of 

```rust
for (u, v, &w) in g1.all_edges() {
    println!("{} {} {} 1", u, v, w);
}
```

I used 

```rust
let mut out = io::BufWriter(io::stdout());
for (u, v, &w) in g1.all_edges() {
    itoa::write(out, u);
    out.write(*" ");
    // ... idem for v and w;
}
```

That allowed me to write only once in 2k characters to `stdout`, and gave a good speed-up. 
Not using `println` gave also a speedup because it does not need all the complex `fmt!` process.  
Coming from Python, where printing is not very costly, this was a surprise.
It also gave an interesting bug, because writes were not on complete lines, so in a parallel configuration, the writes were interleaved.

### I optimized the de-duplicating of edges that were produced from each node.
At first, I regenerated a Graph at the end of the build, thus eliminating the duplicated edges.
That allowed me to test easily my code: in a test configuration, I don't print the output, but test that every pair
of node has the right distance (see [earlier](#python)).
But this operation is costly, and cannot be easily parallelized.
There is another solution to delete duplicates: sort than remove consecutive repeated elements. 
Fortunately, the most costly operation here is sorting, which is implemented by `rayon` in a parallel way.
This way we can reduce this operation to ~20s for `corse.txt`.
After this optimization, the whole execution of the program uses 20s, except for writing to `stdout`, as shown in 
the following graph produced by [`perf`](https://perf.wiki.kernel.org/index.php/Main_Page) and
 [`hotspot`](https://github.com/KDAB/hotspot): 
 
![](./build-time-line.png)

This graph shows the cpu usage per thread, orange corresponds to ~100%, green to near ~0% [^3].
The x-axis is the time, in seconds  
There are 4 periods here:

 - The first part of this figure, from 0s to 50s, use 8 threads to compute the edges starting from each node. 
    `rayon::ParallelIterator` splits the jobs into the correct number of threads to span every core, see [earlier](#buffered-stdout).
 - The second part, from 50s to 55s, is the data flattening of the resulted arrays,
    ie the array of the produced edges for the paths starting at each node.
    This is parallelized into two cores: one for each graph.
    This operation is costly both in time and space: we copy each resulting array
 - The next parts, from 55s to 62s, corresponds to the parallelized sorting of the arrays.
    This is the de-duplicating part.
 - Finally, from 62s to 64s, is the printing of the edges to `stdout` with the [buffered printing](#buffered-stdout).
    Again this cannot be parallelized, because writing to stdout uses a system lock.

[^3]: Precisely, every orange pixel is an perf event, so an orange horizontal bar corresponds too a cpu usage of ~100%. 

At the end, the perf usage tells that for `corse.txt`, the program spends 75% of the cpu-cycles to build the edges
starting from a node. Most of that time is spent to find the node with minimum rho along a path, but 10% of that time is
spent to compute Dijkstra (that means 6% of cpu-cycles are spent doing dijkstra).

## What could be done

#### Using custom graph types in the oracle. 
I use a custom graph implementation that combines neighbor lists and edge HashMap in order to have constant time 
lookup which is important for the building part.
However, for the oracle part this is not needed.
Recall the oracle implementation : 

```rust
pub fn hopsets_oracle(g1: &Graph, g2: &Graph, u: Node, v: Node) -> Distance {
    // ...
    for (_, x, d_ux) in g1.edges(u)
        for (_, y, d_vy) in g1.edges(v)
            if g2.contains_edge(x, y)
                // ...

    // ...
}
```

We see that `g2` only need to support edge HashMap (ie the `contains_edge()` method), and `g1` only neighbor lists
(ie the `edges(u: Node)` method). Furthermore, when parsing the graphs (which may be big), we need to insert into 
these structures every edge. That can take a lot of time, for example, for the `corse.txt` dataset, with the graphs 
built by this program, it represented 66% of the parsing time according to the profiling tool `perf`.
    
#### Find a way to allocate the right size at the beginning of the parsing.
The `distorac` command spends a lot of time rebuilding the two graphs, and a non negligible part of it is used to 
increase the size of the graphs while adding edges.
A solution would be to store the size of the graph as header of the hopsets file, or even to keep the graphs in 
memory. 

# Conclusion

I conclusion, I believe to have produced a code that is both correct and performant.
Speed has greatly improved since my Python version, and even the speed up between my first Rust implementation and my
last version is consequent.
Correctness remained ensured by the integration tests that I wrote, and that allowed me to focus only on performance
while being sure that I did not break anything.

I did stumble on some difficulties, both on the Python side while trying to speed it up, and in Rust, where interesting
bugs popped out.
