use rhopsets::builder::*;
use rhopsets::graph::Graph;
use rhopsets::graph::{generate_circle, generate_grid};
use rhopsets::oracle::*;
use rhopsets::parser::parse_graph;

use rayon::prelude::*;

fn compare_all_entries(graph: &Graph, g1: &Graph, g2: &Graph) {
    (0..graph.node_count())
        .into_par_iter()
        .panic_fuse()
        .zip(0..graph.node_count())
        .for_each(|(u, v)| {
            let expected_result =
                dijkstra_oracle(graph, u, v).expect("Oracle should return a value");
            let result = hopsets_oracle(g1, g2, u, v);
            assert_eq!(result, expected_result, "Wrong distance for {} -> {}", u, v);
        })
}

#[test]
fn compare_on_small_circle() {
    let n = 50;
    let graph = generate_circle(n);

    let (g1, g2) = build_three_hopsets(&graph, 120.0, 0.3).expect("Cannot analyse this shit.");

    compare_all_entries(&graph, &g1, &g2);
}

#[test]
fn compare_on_big_circle() {
    let n = 50;
    let graph = generate_circle(n);

    let (g1, g2) = build_three_hopsets(&graph, 2.0, 0.3).expect("Cannot analyse this shit.");

    compare_all_entries(&graph, &g1, &g2);
}

#[test]
fn compare_on_grid() {
    let n = 10;
    let graph = generate_grid(n);

    let (g1, g2) = build_three_hopsets(&graph, 2.0, 0.3).expect("Build failed");

    compare_all_entries(&graph, &g1, &g2);
}

#[test]
#[ignore]
fn compare_on_corse() {
    // ATTENTION, VEEEEEERRYYYYYYY LOOOOOOONNNNG TEST
    let graph = parse_graph("./assets/corse.txt").expect("Cannot parse the file");

    let (g1, g2) = build_three_hopsets(&graph, 3001002.0, 0.3).expect("Failed to build");

    compare_all_entries(&graph, &g1, &g2);
}
