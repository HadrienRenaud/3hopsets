default: build

build:
	RUSTFLAGS="-C target-cpu=native" cargo build --release

doc:
	cargo doc --document-private-items --open

test:
	cargo run test --release
